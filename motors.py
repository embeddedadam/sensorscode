from LLC_motor import LLC_motor
import pigpio
import wavePWM
import time

class motors:
    def __init__(self, frequency = 25000):
        self.pi = pigpio.pi() 
        self.pwm = wavePWM.PWM(self.pi)
        self.pwm.set_frequency(frequency)

        self.motor1 = LLC_motor(name="motor1")
        self.motor2 = LLC_motor(name="motor2")
        self.motor3 = LLC_motor(name="motor3")
        self.motor4 = LLC_motor(name="motor4")

    def set_speed_motor1(self, pwm_width):
        if not self.motor1.is_active():
            self.motor1.wake()
        if (-1 <= pwm_width <= 1):
            if pwm_width >= 0:
                self.motor1.forward()
            else:
                pwm_width = -1.0 * pwm_width
                self.motor1.backward()
            self.pwm.set_pulse_length_in_fraction(self.motor1.pins["PWM"], pwm_width)
            self.pwm.update()
        else:
            ValueError("Wartość sygnału z poza dozwolonego zakresu! --> <-1, 1>")
        
    def set_speed_motor2(self, pwm_width):
        if not self.motor2.is_active():
            self.motor2.wake()
        if (-1 <= pwm_width <= 1):
            if pwm_width >= 0:
                self.motor2.forward()
            else:
                pwm_width = -1.0 * pwm_width
                self.motor2.backward()
            self.pwm.set_pulse_length_in_fraction(self.motor2.pins["PWM"], pwm_width)
            self.pwm.update()
        else:
            ValueError("Wartość sygnału z poza dozwolonego zakresu! --> <-1, 1>")

    def set_speed_motor3(self, pwm_width):
        if not self.motor3.is_active():
            self.motor3.wake()
        if (-1 <= pwm_width <= 1):
            if pwm_width >= 0:
                self.motor3.forward()
            else:
                pwm_width = -1.0 * pwm_width
                self.motor3.backward()
            self.pwm.set_pulse_length_in_fraction(self.motor3.pins["PWM"], pwm_width)
            self.pwm.update()
        else:
            ValueError("Wartość sygnału z poza dozwolonego zakresu! --> <-1, 1>")

    def set_speed_motor4(self, pwm_width):
        if not self.motor4.is_active():
            self.motor4.wake()
        if (-1 <= pwm_width <= 1):
            if pwm_width >= 0:
                self.motor4.forward()
            else:
                pwm_width = -1.0 * pwm_width
                self.motor4.backward()
            self.pwm.set_pulse_length_in_fraction(self.motor4.pins["PWM"], pwm_width)
            self.pwm.update()
        else:
            ValueError("Wartość sygnału z poza dozwolonego zakresu! --> <-1, 1>")
    
    def clean(self):
        self.pwm.cancel()
