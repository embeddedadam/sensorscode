import time

import gpiozero


class LLC_encoder:
    def __init__(self, name: str):
        self.name = name
        '''
                        PIN_A|PIN_B
        '''
        self.encoders = {
            "encoder1" : (21, 26),
            "encoder2" : (16, 13),
            "encoder3" : (5 , 25),
            "encoder4" : (27, 18)
        }

        self.pins = {
            "PIN_A" : self.encoders[name][0],
            "PIN_B" : self.encoders[name][1],
        }

        self.pins["PIN_A"] = gpiozero.InputDevice(self.pins["PIN_A"])
        self.pins["PIN_B"] = gpiozero.InputDevice(self.pins["PIN_B"])

        self.gear = 72
        self.aState = False
        self.bState = False
        self.aLastState = False
        self.bLastState = False
        self.counter = 0

    def read_rotations(self):
        return self.counter / self.gear

    def reset(self):
        self.counter = 0

    def read_counter(self):
        self.aLastState = self.pins["PIN_A"].value

        while True:
            # print(self.pins["PIN_A"])
            # print(self.pins["PIN_B"])
            self.aState = self.pins["PIN_A"].value
            self.bState = self.pins["PIN_B"].value
            if self.aLastState != self.aState:
                if self.pins["PIN_B"].value != self.aState:
                    self.counter -= 1
                else:
                    self.counter += 1
                print(self.counter / self.gear)

            if self.bLastState != self.bState:
                if self.pins["PIN_A"].value == self.bState:
                    self.counter -= 1
                else:
                    self.counter += 1
                print(self.counter / self.gear)

            self.bLastState = self.bState
            self.aLastState = self.aState
