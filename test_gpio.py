import time
import pigpio
import math

GPIO=4
GPIO1=22
frequency = 12500
T = 1000000/frequency
duty = 0.1

t_on = duty*T

square = []
square1 = []

square.append(pigpio.pulse(1<<GPIO, 0, int(round(t_on/2, 0))))
square.append(pigpio.pulse(0, 1<<GPIO, int(T/2-round(t_on/2, 0))))

square1.append(pigpio.pulse(1<<GPIO1, 0, int(round(t_on/2, 0))))
square1.append(pigpio.pulse(0, 1<<GPIO1, int(T/2-round(t_on/2, 0))))

pi = pigpio.pi()

pi.set_mode(GPIO, pigpio.OUTPUT)
pi.set_mode(GPIO1, pigpio.OUTPUT)

pi.wave_add_generic(square)
wid = pi.wave_create()

pi.wave_add_generic(square1)
wid1 = pi.wave_create()

pi.wave_send_repeat(wid)
pi.wave_send_repeat(wid1)

time.sleep(10)
pi.wave_tx_stop()
pi.wave_delete(wid1)
pi.wave_clear()
