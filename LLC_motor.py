import board
import busio
from gpiozero import DigitalOutputDevice
from digitalio import Direction
from adafruit_mcp230xx.mcp23017 import MCP23017
from gpiozero import MCP3208
import digitalio

i2c = busio.I2C(board.SCL, board.SDA)
MCP18 = MCP23017(i2c)

'''
Low level motor control class.
Provide pinout numbering for each instance of LLC_motor class.
'''


class LLC_motor:
    def __init__(self, name : str):
        self.name = name
        self.pwm = None
        """
                        DIR|PWM|SLP|FLT|CS|PWR
        """
        self.motors = {
            "motor1" : (19, 20,  8,  7,  6,  9),
            "motor2" : (12,  6, 10,  6,  5, 11),
            "motor3" : (23, 22, 12,  5,  7, 13),
            "motor4" : (17,  4, 14,  4,  4, 15)
        }

        self.pins = {
            "DIR" : self.motors[name][0],
            "PWM" : self.motors[name][1],
            "SLP" : self.motors[name][2],
            "FLT" : self.motors[name][3],
            "CS"  : self.motors[name][4],
            "PWR" : self.motors[name][5]
        }

        self.set_pins()


    def set_pins(self):
        self.pins["DIR"] = DigitalOutputDevice(self.pins["DIR"], active_high=False)
        self.pins["SLP"] = MCP18.get_pin(self.pins["SLP"])
        self.pins["FLT"] = MCP18.get_pin(self.pins["FLT"])
        self.pins["CS"] = MCP3208(self.pins["CS"])
        self.pins["PWR"] = MCP18.get_pin(self.pins["PWR"])


        self.pins["SLP"].pull = digitalio.Pull.UP
        self.pins["SLP"].switch_to_output(False)
        self.pins["PWR"].switch_to_input()

    def sleep(self):
        self.pins["SLP"].switch_to_output(False)

    def wake(self):
       self.pins["SLP"].switch_to_output(True)

    def is_active(self):
        return self.pins["PWR"].value

    def forward(self):
        self.pins["DIR"].on()

    def backward(self):
        self.pins["DIR"].off()
    
    def coast(self):
        self.sleep()

    def set_pigpio(self, pwm):
        self.pwm = pwm
        self.pwm.set_pulse_length_in_fraction(self.pins["PWM"], 0)

    def set_speed(self, pwm_width):
        if not self.is_active():
            self.wake()
        if (-1 < pwm_width < 1):
            if pwm_width >= 0:
                self.forward()
            else:
                pwm_width = -1.0 * pwm_width
                self.backward()
            self.pwm.set_pulse_length_in_fraction(self.pins["PWM"], pwm_width)
            self.pwm.update()
        else:
            ValueError("Wartość sygnału z poza dozwolonego zakresu! --> <-1, 1>")

    def brake(self):
        self.pwm.set_pulse_length_in_fraction(self.pins["PWM"], 0)
        self.pwm.update()